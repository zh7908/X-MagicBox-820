module(...,package.seeall)

--当前的开关状态
local isEnable
local isConnected
local enable,disable

function update()
    disp.clear()
    disp.setfontheight(32)
    lcd.CHAR_WIDTH = 16
    lcd.putStringCenter("蓝牙音箱",120,lcd.gety(0),255,255,255)

    lcd.putStringCenter("当前状态",120,lcd.gety(3),255,150,255)
    lcd.putStringCenter(isEnable and "开" or "关",120,lcd.gety(5),255,0,0)

    disp.setfontheight(16)
    lcd.CHAR_WIDTH = 8

    lcd.putStringCenter("开启后，手机搜索“LuatBox”",120,lcd.gety(3),100,100,100)
    lcd.putStringCenter("连接后，可控制上一曲下一曲",120,lcd.gety(14),100,100,100)
end

local keyEvents = {
    ["3"] = function ()
        if isEnable then
            disable()
        else
            enable()
        end
        --开始或者结束播放
    end,
    ["4"] = function ()
        if isConnected then
            btcore.setavrcpsongs(2)--上一曲
        end
    end,
    ["5"] = function ()
        if isConnected then
            btcore.setavrcpsongs(3)--下一曲
        end
    end,
    ["C"] = function ()
        if isConnected then
            btcore.setavrcpsongs(1)--播放
        end
    end,
    ["D"] = function ()
        if isConnected then
            btcore.setavrcpsongs(0)--暂停
        end
    end,
}
keyEvents["A"] = keyEvents["3"]
keyEvents["OK"] = keyEvents["3"]
keyEvents["LEFT"] = keyEvents["4"]
keyEvents["UP"] = keyEvents["4"]
keyEvents["RIGHT"] = keyEvents["5"]
keyEvents["DOWN"] = keyEvents["5"]

function key(k,e)
    if not e then return end
    if keyEvents[k] then
        keyEvents[k]()
        page.update()
    end
end

function close()
    disable()
end

enable = function ()
    if isEnable then return end
    isEnable = true
    sys.taskInit(function ()
        btcore.open(2) --打开蓝牙
        sys.waitUntil("BT_OPEN_IND", 5000)
        if not isEnable then return end
        btcore.setname("LuatBox")-- 设置广播名称
        btcore.setvisibility(0x11)-- 设置蓝牙可见性
        audio.play(0, "TTS", "蓝牙已开启",nvm.get("vol"))
        while isEnable do
            local _, result = sys.waitUntil("BT_CONNECT_IND")
            if not isEnable then return end
            audio.play(0, "TTS", "蓝牙已连接",nvm.get("vol"))
            if not isEnable then return end
            if result then
                btcore.setavrcpvol(100)
                sys.waitUntil("BT_DISCONNECT_IND")
                audio.play(0, "TTS", "蓝牙已断开",nvm.get("vol"))
            end
        end
    end)
end

disable = function ()
    if isEnable then
        isEnable = false
        sys.publish("BT_OPEN")
        sys.publish("BT_AVRCP_CONNECT_IND")
        btcore.close()
        audio.play(0, "TTS", "蓝牙已关闭",nvm.get("vol"))
    end
end

rtos.on(rtos.MSG_BLUETOOTH, function(msg)
    if msg.event == btcore.MSG_OPEN_CNF then
        sys.publish("BT_OPEN_IND", msg.result) --蓝牙打开成功
    elseif msg.event == btcore.MSG_BT_AVRCP_CONNECT_IND then
        log.info("bt", "bt avrcp connect")
        sys.publish("BT_CONNECT_IND", msg.result) --avrcp连接成功
        isConnected = true
    elseif msg.event == btcore.MSG_BT_AVRCP_DISCONNECT_IND then
        log.info("bt", "bt avrcp disconnect") --avrcp断开连接
        sys.publish("BT_DISCONNECT_IND", msg.result)
        isConnected = nil
    end
end)
