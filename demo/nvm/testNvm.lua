--- 模块功能：参数存储功能测试.
-- @author openLuat
-- @module nvm.testNvm
-- @license MIT
-- @copyright openLuat
-- @release 2018.03.27

module(...,package.seeall)

require"config"
require"nvm"

nvm.init("config.lua",false)   -- burnSave 本地烧录是否保留已有参数，true为保留，false或者nil为清除

local function testnvm()
    sys.wait(5000)  -- 延时5s等待USB虚拟串口准备好打印
    -- 读取nvm中的初始化数据
    log.info("nvm.读取默认值", nvm.get("name"))
    log.info("nvm.读取默认值", nvm.get("age"))
    log.info("nvm.读取默认值", nvm.get("sex"))
    local hobby = nvm.get("hobby")
    log.info("nvm.读取默认值", hobby[1],hobby[2])
    local some = nvm.get("something")
    log.info("nvm.读取默认值", some["a"])
    sys.wait(1000)

    log.info("nvm.修改", nvm.sett("hobby",1,"read"))
    log.info("nvm.修改", nvm.sett("something","a",235))
    local hobby2 = nvm.get("hobby")
    local so = nvm.get("something")
    log.info("nvm.更新数据", hobby2[1],so["a"])
    nvm.restore()   -- 恢复出厂
end

sys.taskInit(testnvm)
